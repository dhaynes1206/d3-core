<?php
/*
Plugin Name: D3 Core
Plugin URL: http://d3corp.com
Description: Core functions
Version: 1.0
Author: D3Corp
Author URI: http://d3corp.com
*/

if(!class_exists('D3Core')){
	class D3Core {
		public function __construct(){
			add_action('init', array(&$this,'loader'));
			add_action('after_setup_theme', array(&$this,'darwinSetup'));
			add_action('init', array(&$this,'darwinCore'));
		}

		public function loader(){
			$darwin_includes = [
				'lib/class-tgm-plugin-activation.php',
				'lib/admin.php',
				'lib/theme-options.php',
				'lib/theme-shortcodes.php'
			];

			foreach ($darwin_includes as $file) {
				if (!$filepath = dirname(__FILE__) . '/' . ($file)) {
					trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
				}

				require_once $filepath;
			}
			unset($file, $filepath);
		}

		public function darwinSetup() {
			add_filter( 'wp_handle_upload', array(&$this,'darwin_wp_handle_upload' ));
			add_filter( 'wp_get_attachment_url', array(&$this,'darwin_wp_get_attachment_url' ));
			// launching operation cleanup
		    add_action( 'init', array(&$this,'darwin_head_cleanup' ));
		    // A better title
		    // remove WP version from RSS
		    add_filter( 'the_generator', array(&$this,'darwin_rss_version' ));
		    // remove pesky injected css for recent comments widget
		    add_filter( 'wp_head', array(&$this,'darwin_remove_wp_widget_recent_comments_style' ));
		    // clean up comment styles in the head
		    add_action( 'wp_head', array(&$this,'darwin_remove_recent_comments_style' ));
		    // clean up gallery output in wp
		    add_filter( 'gallery_style', array(&$this,'darwin_gallery_style' ));
		    // cleaning up random code around images
		    add_filter( 'the_content', array(&$this,'darwin_filter_ptags_on_images' ));
		    // remove empty paragraph tags
		    add_filter( 'the_content', array(&$this,'remove_empty_p' ));
		    // adds continue reading link to custom posts
		    add_filter( 'get_the_excerpt', array(&$this,'darwin_custom_excerpt_more' ));
		    // enable shortcodes in our widgets, no php jack!
		    add_filter( 'widget_text', 'do_shortcode');
		    // enqueue base scripts and styles
		    add_action( 'tgmpa_register', array(&$this,'d3themes_required_plugins' ));
		    add_filter( 'screen_layout_columns', array(&$this,'darwin_screen_layout_columns' ));
		    add_filter( 'get_user_option_screen_layout_dashboard', array(&$this,'darwin_screen_layout_dashboard' ));
		}

		public function darwinCore() {
			function is_custom_post_type($post = null) {
				$post_types = get_post_types(array(
					'public'   => true,
					'_builtin' => false,
				), 'names');
				return in_array(get_post_type($post), $post_types);
			}

			function simple_breadcrumb() {
				global $post;
				echo '<ol class="breadcrumb">';
				if (!is_front_page()) {
					ob_start();
					bloginfo('name');
					$blog_name   = ob_get_clean();
					$custom_root = (of_get_option('breadcrumb')) ? of_get_option('breadcrumb') : $blog_name;

					echo '<li><a href="';
					echo get_option('home');
					echo '">';
					echo $custom_root;
					echo "</a></li>";
					if ( is_category() || is_single()) {
						if (get_the_category()) {
							echo "<li>";
							echo the_category(', ');
							echo "</li>";
						}
						if ( is_single() ) {
							if (is_custom_post_type()) {
								$post_type = get_post_type( $post->ID );
								$obj       = get_post_type_object( $post_type );
								if ($obj->has_archive) {
									echo '<li><a href="'.get_option('home').'/'.$obj->rewrite['slug'].'">'.$obj->labels->name.'</a></li>';
								}
							}
							echo "<li>";
							the_title();
							echo "</li>";
						}
					} elseif ( is_page() && $post->post_parent ) {
						$home = get_page_by_title('home');
						for ($i = count($post->ancestors)-1; $i >= 0; $i--) {
							if (($home->ID) != ($post->ancestors[$i])) {
								echo '<li><a href="';
								echo get_permalink($post->ancestors[$i]); 
								echo '">';
								echo get_the_title($post->ancestors[$i]);
								echo "</a></li>";
							}
						}
						echo '<li>'.get_the_title().'</li>';
					} elseif (is_page()) {
						echo '<li>'.get_the_title().'</li>';
					} elseif (is_singular()) {
						echo '<li>'.get_the_title().'</li>';
					} elseif (is_tag()) {
						echo '<li>';
						echo single_tag_title();
						echo '</li>';
					} elseif (is_author()) {
						echo '<li>';
						echo get_the_author();
						echo '</li>';
					} elseif (is_404()) {
						echo "<li>404</li>";
					} elseif (is_post_type_archive()) {
						global $wp_query;
						$obj = $wp_query->get_queried_object();
						echo '<li>'.$obj->labels->name.'</li>';
					}
				} else {
					bloginfo('name');
				}
				echo '</ol>';
			}
		}

		public function darwin_get_relative_attachment_path($path) {
		    $paths = (object)parse_url($path);
		    return $paths->path;
		}

		public function darwin_wp_handle_upload($info) {
		  $info['url'] = darwin_get_relative_attachment_path($info['url']);
		  return $info;
		}

		public function darwin_wp_get_attachment_url($url) {
		    return darwin_get_relative_attachment_path($url);
		}

		public function darwin_screen_layout_columns($columns) {
		    $columns['dashboard'] = 3;
		    return $columns;
		}

		public function darwin_screen_layout_dashboard() { 
		    return 3; 
		}

		public function d3themes_required_plugins() {
		    /*
		     * Array of plugin arrays. Required keys are name and slug.
		     * If the source is NOT from the .org repo, then source is also required.
		     */
		    $plugins = array(
		        array(
		            'name'      => 'Bootstrap Shortcodes',
		            'slug'      => 'bootstrap-shortcodes',
		            'required'  => true,
		        ),
		        array(
		            'name'               => 'GitHub Updater',
		            'slug'               => 'github-updater-5.2.0',
		            'source'             => 'https://github.com/afragen/github-updater/archive/develop.zip', 
		            'required'           => true
		        ),
		         array(
		            'name'               => 'OptionTree',
		            'slug'               => 'option-tree',
		            'required'           => true
		        ),
		        array(
		            'name'               => 'GitHub Link',
		            'slug'               => 'github-link-master',
		            'source'             => 'https://github.com/szepeviktor/github-link/archive/master.zip', 
		            'required'           => false
		        )
		    );

		    /*
		     * Array of configuration settings. Amend each line as needed.
		     *
		     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
		     * strings available, please help us make TGMPA even better by giving us access to these translations or by
		     * sending in a pull-request with .po file(s) with the translations.
		     *
		     * Only uncomment the strings in the config array if you want to customize the strings.
		     */
		    $config = array(
		        'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		        'default_path' => '',                      // Default absolute path to bundled plugins.
		        'menu'         => 'tgmpa-install-plugins', // Menu slug.
		        'parent_slug'  => 'themes.php',            // Parent menu slug.
		        'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		        'has_notices'  => true,                    // Show admin notices or not.
		        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		        'message'      => '',                      // Message to output right before the plugins table.
		    );

		    tgmpa( $plugins, $config );
		}

		public function darwin_head_cleanup() {
			remove_action( 'wp_head', 'rsd_link' );
			// windows live writer
			remove_action( 'wp_head', 'wlwmanifest_link' );
			// previous link
			remove_action( 'wp_head', 'parent_post_rel_link' );
			// start link
			remove_action( 'wp_head', 'start_post_rel_link' );
			// links for adjacent posts
			remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head' );
			// WP version
			remove_action( 'wp_head', 'wp_generator' );
			// remove WP version from css
			add_filter( 'style_loader_src', array(&$this,'darwin_remove_wp_ver_css_js' ));
			// remove Wp version from scripts
			add_filter( 'script_loader_src', array(&$this,'darwin_remove_wp_ver_css_js' ));

		} /* end darwin head cleanup */

		// remove WP version from RSS
		public function darwin_rss_version() { return ''; }

		// remove WP version from scripts
		public function darwin_remove_wp_ver_css_js( $src ) {
			if ( strpos( $src, 'ver=' ) )
			$src = remove_query_arg( 'ver', $src );
			return $src;
		}

		// remove injected CSS for recent comments widget
		public function darwin_remove_wp_widget_recent_comments_style() {
			if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
				remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
			}
		}

		// remove injected CSS from recent comments widget
		public function darwin_remove_recent_comments_style() {
			global $wp_widget_factory;
			if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
				remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
			}
		}

		// remove injected CSS from gallery
		public function darwin_gallery_style($css) {
			return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
		}

		// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
		public function darwin_filter_ptags_on_images($content){
			return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
		}

		// This removes the annoying […] to a Read More link
		public function darwin_excerpt_more($more) {
			global $post;
			// edit here if you like
			return '...  <a class="excerpt-read-more" href="'. get_permalink( $post->ID ) . '" title="'. __( 'Read ', 'darwintheme' ) . esc_attr( get_the_title( $post->ID ) ).'">'. __( 'Read more &raquo;', 'darwintheme' ) .'</a>';
		}

		public function darwin_custom_excerpt_more( $output ) {
			if ( has_excerpt() && ! is_attachment() ) {
				$output .= darwin_continue_reading_link();
			}
			return $output;
		}

		public function remove_empty_p($content){
			$content = force_balance_tags($content);
			return preg_replace('#<p>(\s|&nbsp;)*+(<br\s*/*>)?(\s|&nbsp;)*</p>#i', '', $content);
		}
	}

	//initialize the plugin
	$D3Core = new D3Core();
}