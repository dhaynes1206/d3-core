<?php

//////////////////////////////////////////////////////////////////
// White label admin area
//////////////////////////////////////////////////////////////////
add_action( 'admin_bar_menu', 'remove_wp_logo' ,999);
add_action( 'wp_before_admin_bar_render', 'wp_admin_bar_new_item' );
add_action( 'admin_enqueue_scripts', 'darwin_admin_head' );
add_action( 'login_head', 'darwin_admin_head' );
add_action( 'admin_menu','wphidenag' );
add_filter( 'admin_bar_menu', 'replace_howdy' );
add_action( 'wp_dashboard_setup', 'remove_dashboard_widgets' );
add_action( 'wp_dashboard_setup', 'add_dashboard_widgets' );
add_filter( 'admin_footer_text', 'footer_admin' );
add_action( 'admin_menu', 'remove_menus' );
add_filter( 'login_headerurl','darwin_custom_logo_link' );
add_filter( 'login_headertitle', 'darwin_change_title_on_logo' );

function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
	$wp_admin_bar->remove_node( 'updates' );
	$wp_admin_bar->remove_node( 'comments' );
	$wp_admin_bar->remove_node( 'search' );
}

function wp_admin_bar_new_item() {
	global $wp_admin_bar;
	$wp_admin_bar->add_menu(array(
		'id' => 'wp-admin-bar-new-item',
		'title' => __('D3 Specials'),
		'href' => 'http://d3corp.com/monthly-specials/' . date('Y') . '/' . strtolower(date('F')) .'',
		'meta' => array('target' => '_blank')
	));
}

//Load admin theme style sheet and our custom js
function darwin_admin_head() {
	wp_enqueue_style('admin/css', home_url().'/app/plugins/d3-core/assets/css/wp-admin.css');
	wp_enqueue_style('fontawesome','//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css');
}

function wphidenag() {
	remove_action( 'admin_notices', 'update_nag' );
}

function replace_howdy( $wp_admin_bar ) {
    $my_account=$wp_admin_bar->get_node('my-account');
    $newtitle = str_replace( 'Howdy,', 'Hello, welcome back', $my_account->title );
    $wp_admin_bar->add_node( array(
        'id' => 'my-account',
        'title' => $newtitle,
    ) );
}

function remove_dashboard_widgets() {
	// Globalize the metaboxes array, this holds all the widgets for wp-admin
 	global $wp_meta_boxes;

	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);        // Activity Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // Comments Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);  // Incoming Links Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);         // Plugins Widget

	// unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);    // Quick Press Widget
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);     // Recent Drafts Widget
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);           //
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);         //

	// remove plugin dashboard boxes
	unset($wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget']);           // Yoast's SEO Plugin Widget
}

function add_dashboard_widgets() {
	wp_add_dashboard_widget('dashboard_widget', 'D3 Monthly Special!', 'darwin_current_special');
	wp_add_dashboard_widget('get_started', 'Get Started!', 'get_started');
}

function darwin_current_special() {
	// Display our monthly special
	echo "<div style='text-align:center'><a target='_blank' href='http://d3corp.com/monthly-specials/" . date('Y') . "/" . strtolower(date('F')) ."'><img style='width: 100%;' src='http://d3corp.com/images/current-special.jpg' /></a></div>";
} 

function get_started() {
	?>
	<ul>
		<li><a href="/wp-admin/post-new.php" class="ab-item"><span class="fa fa-pencil"></span> New Post</a></li>
		<li><a href="/wp-admin/media-new.php" class="ab-item"><span class="fa fa-photo"></span> Add Media</a></li>
		<li><a href="/wp-admin/post-new.php?post_type=page" class="ab-item"><span class="fa fa-file"></span> New Page</a></li>
		<li><a href="/wp-admin/user-new.php" class="ab-item"><span class="fa fa-user"></span> New User</a></li>
	</ul>
	<?
} 

function footer_admin () {
	_e( '<span id="footer-thankyou">Thank you for using the D3Panel.', 'darwin');
}

function remove_menus () {
global $menu;
//retrieve current user info
     global $current_user;
     get_currentuserinfo();

     //if current user level is less than 10, remove the postcustom meta box
     if ($current_user->user_level < 10) {
		
		//echo '<style> #message { display:none; } </style>';
		$restricted = array(__('Tools'));
		end ($menu);
		while (prev($menu)){
			$value = explode(' ',$menu[key($menu)][0]);
			if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
		}
	 }
}
//no wordpress links
function darwin_custom_logo_link() {
	return get_site_url();
}

function darwin_change_title_on_logo() {
	return get_bloginfo('name');
}