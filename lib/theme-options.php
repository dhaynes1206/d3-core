<?php

/*
 * Loads the Options Panel
 *
*/

add_filter( 'ot_theme_mode', '__return_false' );

//////////////////////////////////////////////////////////////////
// Lets set up our options
//////////////////////////////////////////////////////////////////
add_action( 'init', 'd3_custom_options' );

function d3_custom_options($options=array()){	
	$favicon 	  = get_template_directory_uri().'/images/favicon.ico';
	$ocbut 	  	  = get_template_directory_uri().'/images/oc.png';
	$logo		  = get_template_directory_uri().'/images/admin-logo.png';
	$admin_bg_img = get_template_directory_uri().'/images/admin-bg.jpg';
	/* OptionTree is not loaded yet, or this is not an admin request */
	if ( ! function_exists( 'ot_settings_id' ) || ! is_admin() )
		return false;
	/**
	* Get a copy of the saved settings array. 
	*/
	$saved_settings = get_option( ot_settings_id(), array() );

	/**
	* Custom settings array that will eventually be 
	* passes to the OptionTree Settings API Class.
	*/
	$custom_settings = array( 
		'contextual_help' => array( 
			'content'       => array( 
				array(
					'id'        => 'option_types_help',
					'title'     => __( 'Option Types', 'darwin' ),
					'content'   => '<p>' . __( 'Help content goes here!', 'darwin' ) . '</p>'
				)
			),
			'sidebar'       => '<p>' . __( 'Sidebar content goes here!', 'darwin' ) . '</p>'
		),
		'sections'        => array( 
			array(
				'id'          => 'basic_settings',
				'title'       => __( 'Basic Settings', 'darwin' )
			),
			array(
				'id'          => 'font_settings',
				'title'       => __( 'Font Settings', 'darwin' )
			),
			array(
				'id'          => 'social_settings',
				'title'       => __( 'Social Media', 'darwin' )
			),
			array(
				'id'          => 'admin_settings',
				'title'       => __( 'Admin Settings', 'darwin' )
			)
		),
		'settings'        => array( 
			array(
				'id'          => 'breadcrumb',
				'label'       => __( 'Root Breadcrumb', 'darwin' ),
				'desc'        => __( 'custom breadcrumb text.', 'darwin' ),
				'std'         => 'Site Name',
				'type'        => 'text',
				'section'     => 'basic_settings',
				'operator'    => 'and'
			),
			array(
				'id'          => 'favicon',
				'label'       => __( 'Favicon Image', 'darwin' ),
				'desc'        => __( 'favicon image.', 'darwin' ),
				'std'         => $favicon,
				'type'        => 'upload',
				'section'     => 'basic_settings',
				'operator'    => 'and'
			),
			array(
				'id'          => 'logo',
				'label'       => __( 'Site Logo', 'darwin' ),
				'desc'        => __( 'site logo.', 'darwin' ),
				'std'         => $logo,
				'type'        => 'upload',
				'section'     => 'basic_settings',
				'operator'    => 'and'
			),
			array(
				'id'          => 'biz_name',
				'label'       => __( 'Business Name', 'darwin' ),
				'desc'        => __( 'business name.', 'darwin' ),
				'std'         => 'D3Corp',
				'type'        => 'text',
				'section'     => 'basic_settings',
				'operator'    => 'and'
			),
			array(
				'id'          => 'biz_addy',
				'label'       => __( 'Business Address', 'darwin' ),
				'desc'        => __( 'business address.', 'darwin' ),
				'std'         => '12319 Ocean Gateway, Suite 202',
				'type'        => 'text',
				'section'     => 'basic_settings',
				'operator'    => 'and'
			),
			array(
				'id'          => 'biz_addy2',
				'label'       => __( 'Business City, State, Zip', 'darwin' ),
				'desc'        => __( 'business city, state,.', 'darwin' ),
				'std'         => 'Ocean City, Maryland 21842',
				'type'        => 'text',
				'section'     => 'basic_settings',
				'operator'    => 'and'
			),
			array(
				'id'          => 'email',
				'label'       => __( 'Email Address', 'darwin' ),
				'desc'        => __( 'email address.', 'darwin' ),
				'std'         => 'info@d3corp.com',
				'type'        => 'text',
				'section'     => 'basic_settings',
				'operator'    => 'and'
			),
			array(
				'id'          => 'ocbutton',
				'label'       => __( 'OC Image', 'darwin' ),
				'desc'        => __( 'visit oc image.', 'darwin' ),
				'std'         => $ocbut,
				'type'        => 'upload',
				'section'     => 'basic_settings',
				'operator'    => 'and'
			),
			array(
				'id'          => 'demo_google_fonts',
				'label'       => __( 'Google Fonts', 'darwin' ),
				'desc'        => sprintf( __( 'The Google Fonts option type will dynamically enqueue any number of Google Web Fonts into the document %1$s. As well, once the option has been saved each font family will automatically be inserted into the %2$s array for the Typography option type. You can further modify the font stack by using the %3$s filter, which is passed the %4$s, %5$s, and %6$s parameters. The %6$s parameter is being passed from %7$s, so it will be the ID of a Typography option type. This will allow you to add additional web safe fonts to individual font families on an as-need basis.', 'darwin' ), '<code>HEAD</code>', '<code>font-family</code>', '<code>ot_google_font_stack</code>', '<code>$font_stack</code>', '<code>$family</code>', '<code>$field_id</code>', '<code>ot_recognized_font_families</code>' ),
				'std'         => array( 
					array(
						'family'    => 'opensans',
						'variants'  => array( '300', '300italic', 'regular', 'italic', '600', '600italic' ),
						'subsets'   => array( 'latin' )
					)
				),
				'type'        => 'google-fonts',
				'section'     => 'font_settings',
				'rows'        => '',
				'post_type'   => '',
				'taxonomy'    => '',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
			array(
				'id'          => 'social_links',
				'label'       => __( 'Social media links', 'darwin' ),
				'desc'        => __( 'social media links', 'darwin' ),
				'type'        => 'social-links',
				'section'     => 'social_settings',
				'operator'    => 'and'
			),
			array(
				'id'          => 'social_count',
				'label'       => __( 'Social media share counter on single pages?', 'darwin' ),
				'desc'        => __( 'show or hide', 'darwin' ),
				'type'        => 'on-off',
				'section'     => 'social_settings',
				'operator'    => 'and'
			),
			array(
				'id'          => 'admin_logo',
				'label'       => __( 'Admin Logo', 'darwin' ),
				'desc'        => __( 'admin login logo.', 'darwin' ),
				'std'         => $logo,
				'type'        => 'upload',
				'section'     => 'admin_settings',
				'operator'    => 'and'
			),
			array(
				'id'          => 'admin_logo_height',
				'label'       => __( 'Logo Height', 'darwin' ),
				'desc'        => __( 'admin logo height.', 'darwin' ),
				'std'         => '115px',
				'type'        => 'text',
				'section'     => 'admin_settings',
				'operator'    => 'and'
			),
			array(
				'id'          => 'admin_logo_width',
				'label'       => __( 'Logo Width', 'darwin' ),
				'desc'        => __( 'admin logo width.', 'darwin' ),
				'std'         => '320px',
				'type'        => 'text',
				'section'     => 'admin_settings',
				'operator'    => 'and'
			),
			array(
				'id'          => 'admin_bg_img',
				'label'       => __( 'Admin BG Image', 'darwin' ),
				'desc'        => __( 'admin login bg.', 'darwin' ),
				'type'        => 'background',
				'section'     => 'admin_settings',
				'std'		  => $admin_bg_img,
				'operator'    => 'and'
			),
			array(
				'id'          => 'login_bg_color',
				'label'       => __( 'Admin Login Box BG', 'darwin' ),
				'desc'        => __( 'admin login bg.', 'darwin' ),
				'type'        => 'colorpicker',
				'section'     => 'admin_settings',
				'std'         => '#fff',
				'operator'    => 'and'
			),
			array(
				'id'          => 'login_bttn_bg_color',
				'label'       => __( 'Admin Buttons Background', 'darwin' ),
				'desc'        => __( 'admin login button color.', 'darwin' ),
				'type'        => 'colorpicker',
				'section'     => 'admin_settings',
				'std'         => '#2077aa',
				'operator'    => 'and'
			), array(
				'id'          => 'login_bttn_bg_hover_color',
				'label'       => __( 'Admin Buttons Background Hover', 'darwin' ),
				'desc'        => __( 'admin login button hover.', 'darwin' ),
				'type'        => 'colorpicker',
				'section'     => 'admin_settings',
				'std'         => '#4EA7DB',
				'operator'    => 'and'
			),
		)
  	);
  
	/* allow settings to be filtered before saving */
	$custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );

	/* settings are not the same update the DB */
	if ( $saved_settings !== $custom_settings ) {
	update_option( ot_settings_id(), $custom_settings ); 
	}

	/* Lets OptionTree know the UI Builder is being overridden */
	global $ot_has_custom_theme_options;
	$ot_has_custom_theme_options = true;
}

//////////////////////////////////////////////////////////////////
// Custom admin styles
//////////////////////////////////////////////////////////////////
function custom_logo() {
	$custom_logo   	     = ot_get_option('admin_logo');	
	$admin_bg_color	     = ot_get_option('admin_bg_color');
	$admin_logo_height   = ot_get_option('admin_logo_height');
	$admin_logo_width    = ot_get_option('admin_logo_width');
	$adminbg = ot_get_option('admin_bg_img');
	$login_bg	   	     = ot_get_option('login_bg_color');
	$login_bttn_bg 	     = ot_get_option('login_bttn_bg_color');
	$login_bttn_bg_hover = ot_get_option('login_bttn_bg_hover_color');

	echo "<style>
		.login h1 a {
			background: url(".$custom_logo.") no-repeat;
			height: ".$admin_logo_height." !important;
			width: ".$admin_logo_width." !important;
		}
		#login {
			width: ".$admin_logo_width." !important;
		}
		body.login {
			background-image: url(".$adminbg['background-image'].");
			background-color: ".$adminbg['background-color'].";
			background-repeat: ".$adminbg['background-repeat'].";
			background-attachment: ".$adminbg['background-attachment'].";
			background-size: ".$adminbg['background-size'].";
			background-position: ".$adminbg['background-position'].";
		}
		.login form {
			background: ".$login_bg.";	
		}
		.login #nav, .login #backtoblog {
			background:	 ".$login_bttn_bg.";
		}
		.login #nav:hover, .login #backtoblog:hover {
			background:	 ".$login_bttn_bg_hover.";
		}
	</style>";
}

add_action('login_head', 'custom_logo');