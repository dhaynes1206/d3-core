<?php
// ---------------- SHORTCODES ----------------

add_shortcode('date', 'shortcode_date');
function shortcode_date($atts, $content = null) {
	return date('Y');
}

add_shortcode('clear', 'shortcode_clear');
function shortcode_clear($atts, $content = null) {
	return '<div class="clearfix"></div>';
}

add_shortcode('copyright', 'shortcode_copyright');
function shortcode_copyright($atts, $content = null) {
	extract(shortcode_atts(array(
		'name'		=> ot_get_option('biz_name')
    ), $atts));
	$out .= '<div class="copy">';
	$out .= 'Copyright &copy; '.date('Y').' <a href="/">'. $name .'</a> All Rights Reserved <br />';
	$out .= '<a href="http://d3corp.com" target="_blank">Website Design</a> by <a href="http://d3corp.com" target="_blank">D3Corp</a> <a href="http://visitoceancity.com" target="_blank">Ocean City Maryland</a>';
	$out .= '</div>';
	return $out;
}

add_shortcode('fa', 'shortcode_fa');
function shortcode_fa($atts, $content = null) {
	 extract(shortcode_atts(array(
		'icon'		=> ''
    ), $atts));
	return '<span class="fa fa-'.$icon.'"></span>';
}

add_shortcode('option', 'shortcode_option');
function shortcode_option($atts, $content = null) {
	 extract(shortcode_atts(array(
		'name'		=> ''
    ), $atts));
	return ot_get_option($name);
}

add_shortcode('gmap', 'shortcode_gmap');
function shortcode_gmap($atts, $content = null) {
	extract(shortcode_atts(array(
		'height' => '',
		'width'  => ''
    ), $atts));
	$address = ot_get_option('biz_addy');
	$city    = ot_get_option('biz_addy2');
	$destination = urlencode($address .' '. $city);
	return '<iframe id="gmap" width="'.$width.'" height="'.$height.'" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q='.$destination.'&key=AIzaSyAs4XrBB2RVDz5PClLy6QL_UO5unhFJZv4" allowfullscreen></iframe>';
}

//////////////////////////////////////////////////////////////////
// Email shortcode
//////////////////////////////////////////////////////////////////
function displayJSEmail($atts) {
   extract(shortcode_atts(array('address'=>'#','text'=>''),$atts));
   
   $email_exploded = str_split($address,3);
   $email_str = "'" . implode("'+'", $email_exploded) . "'";
   
   $text = ($text) ? $text : $address;
   
   $id = uniqid();
   ob_start();
   ?>
   <a id="<?=$id;?>" href="#"></a>
   <script type="text/javascript">;
	   var emtx = <?=$email_str;?>;
	   document.getElementById('<?=$id;?>').setAttribute('href', 'mailto:'+emtx);
	   document.getElementById('<?=$id;?>').innerHTML = '<?=$text;?>';
   </script><?php
   return ob_get_clean();
}

add_shortcode('email', 'displayJSEmail');

//////////////////////////////////////////////////////////////////
// Internal Anchor shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('anchor','showAnchor');
  
function showAnchor($atts) {
    extract(shortcode_atts(array(
        'id'		=> 1,
        'text'		=> '',  // default value if none supplied
		'type'		=> '',
		'target'	=> '',
		'class'		=> ''
    ), $atts));
      
    if ($type == 'link') {
        $url = $target;
        return "<a class='$class' href='$url' target='_blank'>$text</a>";
    } else if ($type == 'phone') {
		$url = $target;
        return "<a class='$class' href='tel:$url'>$text</a>";
	} else if ($type == 'email') {
		$url = $target;
        return "<a class='$class' href='mailto:$url'>$text</a>";
	} else if ($id) {
		$url = get_permalink($id);
        return "<a class='$class' href='$url'>$text</a>";
	}
	
}
	
//////////////////////////////////////////////////////////////////
// Button shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('button', 'shortcode_button');
function shortcode_button($atts, $content = null) {
	extract(shortcode_atts(array('link' => '#', 'class' => '', 'target' => '_self'), $atts));
	return '<a class="btn btn-default '.$class.'" href="'.$link.'" target="'.$target.'">' . do_shortcode($content) . '</a>';
}


//////////////////////////////////////////////////////////////////
// Display Custom Post Type
//////////////////////////////////////////////////////////////////
add_shortcode('d3_custom_post_type','d3_display_custom_post_type');

function d3_display_custom_post_type($atts, $content = null){
	//setup default attributes
	$default_atts = array(
		'type'				=> 'post',				//the post type we want to get posts for
		'status'			=> 'publish',			//get only published status
		'limit'				=> -1,					//get all of the posts
		'cat'				=> '',					//specify categories by id comma separated (ex. 1,2,3)
		'order'				=> 'DESC',				//which direction to display them (DESC = newest first, ASC = oldest first)
		'orderby'			=> 'date',				//what to order by (please see documentation for doing it by a meta key)
		'sticky_posts'		=> 1,					//this will tell it not to put stick posts at the top and instead in there normal sort order
		'template'			=> 'default.php'		//this is the template it will use to display
	);
	
	//allow for adding an attribute
	$default_atts = apply_filters('d3_custom_post_type_atts',$default_atts);
	
	//setup some options that will allow for the user to override (reference here -> https://codex.wordpress.org/Class_Reference/WP_Query)
    $options = shortcode_atts($default_atts,$atts);
	
	//setup the arguments to send to it
	$args = array(
		'post_type'				=> $options['type'],
		'post_status'			=> $options['status'],
		'posts_per_page'		=> $options['limit'],
		'cat'					=> $options['cat'],
		'order'					=> $options['order'],
		'orderby'				=> $options['orderby'],
		'ignore_stick_posts'	=> $options['sticky_posts']
	);
	
	//now lets get the paging if it is limited
	if($args['posts_per_page'] != -1){
		$args['paged'] = (get_query_var('paged')) ? get_query_var('paged') : 1;
	}
	
	//apply a filter to the arguments so someone could override if they wanted to
	$args = apply_filters('d3_custom_post_type_args',$args,$options);

	//lets locate the template to start off with
	$template = locate_template('tpl/'.$options['template']);
	
	//there is no template so lets just get out of here (we could put a message here if needed)
	if(!$template) return '';
	
	//lets go ahead and make the query
	$loop = new WP_Query($args);

	//start an output buffer (reason being is that shortcodes return the content rather than outputting)
    ob_start();
    
	//lets include the template
	include($template);
	
	//lets just return the content so that it can display
	return ob_get_clean();
}

//////////////////////////////////////////////////////////////////
/* Sample filter for custom attributes
add_filter('d3_custom_post_type_atts','my_custom_atts');
function my_custom_atts($atts){
	$atts['listing_taxonomy'] = '';
	return $atts;
}
*/

/* Sample filter for custom args
add_filter('d3_custom_post_type_args','my_custom_args',10,2);
function my_custom_args($args,$options){
	if($options['listing_taxonomy']){
		$args['tax_query'][] = array(
			'taxonomy'	=> 'listing-taxonomy',
			'field'		=> 'id',
			'terms'		=> explode(',',$options['listing_taxonomy'])
		);
	}
	return $args;
}
*/

//////////////////////////////////////////////////////////////////
// Social Counters Display
//////////////////////////////////////////////////////////////////
add_shortcode('social-counter', 'social_shares');
function social_shares($atts) {
	global $wpdb;
	
	// extract. nice what is this 3rd grade?
	extract(shortcode_atts(array(
        'fb'		=> '',
        'twitter'	=> '',  
		'gplus'		=> '',
		'pinterest'	=> '',
		'linkedin'	=> ''
    ), $atts));
	
	
	$table_name = $wpdb->prefix . 'sharedcount';
	
	// firwst lets make sure we have a pwetty wittle tabew to store our info in
	if ($wpdb->get_var("SHOW TABLES LIKE '".$table_name."'") != $table_name) {
		$sql = "CREATE TABLE `".$table_name."` (
			`url` varchar(255) NOT NULL,
			`data` text,
			`cache_time` int(10) unsigned default NULL,
			PRIMARY KEY  (`url`)
		  ) ENGINE=MyISAM;";
		
		require_once ABSPATH . 'wp-admin/includes/upgrade.php';
		dbDelta($sql);
	}

	$url = get_permalink( $post_id ); // get our current url
	$newurl = rtrim($url,'/');
	$json = 'https://free.sharedcount.com/?url='.rawurlencode($newurl).'&apikey=c185086577b59a7a0a74c9fec8f300134c6272a2';
	
	$interval = 7200;
	
	$last = $wpdb->get_var("SELECT cache_time FROM `".$table_name."` WHERE url = '".esc_sql($url)."'");

	$now = time();
	
	// check the cache
	if ( !$last || (( $now - $last ) > $interval) ) {
		// cache doesn't exist, or is old, so refresh it
		if( function_exists('curl_init') ) { // if cURL is available, use it...
			$ch = curl_init($json);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_TIMEOUT, 10);
			$cache = curl_exec($ch);
			curl_close($ch);
		} else {
			$cache = file_get_contents($json); // ...if not, use the common file_get_contents()
		}
		
		if ($cache) {
			$wpdb->replace($table_name, array(
				'url'		=> $url,
				'data'		=> $cache,
				'cache_time' => $now
			));		
		}
	}
	
	// read data from database
	$social_data = $wpdb->get_var("SELECT data FROM `".$table_name."` WHERE url = '".esc_sql($url)."'");
	
	if ($social_data) {	
		$counts = json_decode($social_data, true); 
		$twit= $counts["Twitter"]; // twitter count
		$facebook = $counts["Facebook"]["total_count"]; // fb count
		$google = $counts["GooglePlusOne"]; // gplus count
		$pin = $counts["Pinterest"]; // pinterest count
		$linked = $counts["LinkedIn"]; // linkedin count
		$total_count = $twit + $facebook + $google + $pin + $linked;

		echo '<div class="social-counter">';
		echo '<span class="total-count"><span>'.$total_count.'</span> shares</span>';

		if($atts['fb'] == 'yes') { 
			echo '<span class="facebook"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u='.urlencode(get_permalink($post->ID)).'"><i class="fa fa-facebook"></i><span class="counter">'.$facebook.'</span></a></span>';
		}
		if($atts['twitter'] == 'yes') { 
			echo '<span class="twitter"><a target="_blank" href="http://twitter.com/intent/tweet?text='.urlencode(get_the_title()).'&url='.urlencode(get_permalink($post->ID)).'"><i class="fa fa-twitter"></i><span class="counter">'.$twit.'</span></a></span>';
		}
		if($atts['gplus'] == 'yes') { 
			echo '<span class="gplus"><a target="_blank" href="https://plus.google.com/share?url='.urlencode(get_permalink($post->ID)).'"><i class="fa fa-google-plus"></i><span class="counter">'.$google.'</span></a></span>';
		}
		if($atts['pinterest'] == 'yes') { 
			echo '<span class="pinterest"><a href="javascript:void((function(d)%7Bvar%20e%3Dd.createElement(%27script%27)%3Be.setAttribute(%27type%27,%27text/javascript%27)%3Be.setAttribute(%27charset%27,%27UTF-8%27)%3Be.setAttribute(%27src%27,%27//assets.pinterest.com/js/pinmarklet.js%3Fr%3D%27%2BMath.random()*99999999)%3Bd.body.appendChild(e)%7D)(document))%3B"><i class="fa fa-pinterest"></i><span class="counter">'.$pin.'</span></a></span>';
		}
		if($atts['linkedin'] == 'yes') { 
			echo '<span class="linkedin"><a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url='.urlencode(get_permalink($post->ID)).'&title='.urlencode(get_the_title()).'"><i class="fa fa-linkedin"></i><span class="counter">'.$linked.'</span></a></span>';
		}

		echo '</div>';
	}
}

//////////////////////////////////////////////////////////////////
// List Menus
//////////////////////////////////////////////////////////////////
class D3_Menu_Walker extends Walker_Nav_Menu {
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="' . esc_attr( $class_names ) . '"';

        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= '<span class="title">'.$args->link_before.apply_filters('the_title',$item->title,$item->ID).'</span></a>'.$args->link_after;
        
		if($item->description){
			$item_output .= '<span class="sub"> - '.$item->description.'</span></a>';
		}
        
		$item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args, $id);
		//$output .= '</li>';
    }
}

function list_menu($atts, $content = null) { 
	extract(shortcode_atts(array(   
		'menu'            => '',  
		'container'       => 'div',  
		'container_class' => '',  
		'container_id'    => '',  
		'menu_class'      => 'menu',  
		'menu_id'         => '', 
		'echo'            => true, 
		'fallback_cb'     => 'wp_page_menu', 
		'before'          => '', 
		'after'           => '', 
		'link_before'     => '', 
		'link_after'      => '', 
		'depth'           => 0, 
		'walker'          => new D3_Menu_Walker(), 
		'theme_location'  => ''),  
		$atts)); 
  
  
	return wp_nav_menu( array(  
		'menu'            => $menu,  
		'container'       => $container,  
		'container_class' => $container_class,  
		'container_id'    => $container_id,  
		'menu_class'      => $menu_class,  
		'menu_id'         => $menu_id, 
		'echo'            => false, 
		'fallback_cb'     => $fallback_cb, 
		'before'          => $before, 
		'after'           => $after, 
		'link_before'     => $link_before, 
		'link_after'      => $link_after, 
		'depth'           => $depth, 
		'walker'          => $walker, 
		'theme_location'  => $theme_location)); 
} 

//Create the shortcode 
add_shortcode("listmenu", "list_menu");